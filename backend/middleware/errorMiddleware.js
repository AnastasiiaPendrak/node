function errorHandler(err, req, res, next) {
  if (res.status(400)) {
    res.status(400).json({
      message: "Check your data",
    });
  } else if (res.status(500)) {
    res.status(500).json({
      message: "Server error",
    });
  }
}

module.exports = {
  errorHandler,
};
