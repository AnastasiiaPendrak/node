const path = require("path");
const fs = require("fs");

const checkExtension = (extension) => {
  const allExt = [".html", ".log", ".txt", ".json", ".yaml", ".xml", ".js"];
  let count = 0;

  allExt.forEach((element) => {
    if (extension == element) {
      count = +1;
    }
  });

  if (count === 1) {
    console.log("Extension is correct!");
  } else {
    throw new Error();
  }
};

const getFile = async (req, res) => {
  try {
    const fileFolder = path.join(__dirname, "..", "files");

    if (fs.existsSync(fileFolder)) {
      fs.readdir(fileFolder, (err, files) => {
        if (err) throw err;
        allFiles = files;
        res.status(200).json({
          message: "Success",
          files: allFiles,
        });
      });
    } else {
      res.status(500).json({
        message: "Client error",
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
};

const postFile = (req, res) => {
  const data = req.body;
  const filename = data.filename;
  const extension = path.extname(filename);
  const content = data.content;
  checkExtension(extension);

  const fileFolder = path.join(__dirname, "..", "files");
  const fileFolderWrite = path.join(__dirname, "..", "files", `${filename}`);

  console.log(fileFolderWrite);

  try {
    if (!fs.existsSync(fileFolder)) {
      fs.mkdirSync(fileFolder);
    } else {
      console.log("Check your data");
    }

    fs.writeFile(fileFolderWrite, content, function (err) {
      if (err) {
        throw err;
      }
    });

    return res.status(200).json({
      message: "File created successfully",
    });
  } catch {
    res.status(500).json({ message: "Error" });
  }
};

const getFileByFilename = (req, res) => {
  try {
    const fileFolder = path.join(__dirname, "..", "files");
    const filename = req.params.filename;

    fs.readFile(fileFolder + "/" + filename, "utf8", (err, data) => {
      let extension = path.extname(filename);
      content = data;

      if (fs.existsSync(fileFolder + "/" + filename)) {
        fs.stat(fileFolder + "/" + filename, (error, stats) => {
          uploadedDate = stats.birthtime;

          res.status(200).json({
            message: "Success",
            filename: filename,
            content: content,
            extension: extension.split(".").pop(),
            uploadedDate: uploadedDate,
          });
        });
      } else {
        return res.status(400).json({
          message: `No file with this ${filename} filename found`,
        });
      }
    });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

module.exports = {
  getFile,
  postFile,
  getFileByFilename,
};
