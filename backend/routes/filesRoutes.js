const express = require("express");
const router = express.Router();
const {getFile, postFile, getFileByFilename} = require('../controllers/fileController')


router.route('/').get(getFile).post(postFile)
router.route('/:filename').get(getFileByFilename)

module.exports = router;
